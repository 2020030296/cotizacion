const express = require('express'); //Llamando express
const bodyparser = require('body-parser'); //llamando body-parser


const app = express(); //uso de la funcion express

app.set('view engine', 'ejs'); //Funciones para que el navegador lo lea
app.set(express.static(__dirname + "/public")); //Convirtiendo la carpeta en publica
app.use(bodyparser.urlencoded({extended:true}));//uso del bodyparser

//Metodo get tablas
app.get("/tablas", (req, res)=>{

    const valores={
        tabla:req.query.tabla
    }
res.render('tablas', valores);
});

//Metodo post tablas
app.post("/tablas", (req, res)=>{
    const valores ={
        tabla:req.body.tabla
    }
    res.render('tablas', valores);
});

//Metodo get cotizacion
app.get("/cotizacion", (req, res)=>{

    const valores ={
        
        pInicial: req.query.pInicial,
        plazos: req.query.plazos,
        valor: req.query.valor
    }

    res.render('cotizacion', valores);
});

//Metodo post cotizacion
app.post("/cotizacion", (req, res)=>{

    const valores ={
        pInicial: req.body.pInicial,
        plazos: req.body.plazos,
        valor: req.body.valor
    }

    res.render('cotizacion', valores);
});
//Pagina error
app.use((req,res,next)=>{

    res.status(404).sendFile(__dirname + '/public/error.html')

});
//Escuchar el servidor por el puerto 3000
const puerto =3000;
app.listen(puerto,() =>{
    console.log('Iniciado puerto 3000');
});
